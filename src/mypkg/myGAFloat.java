package mypkg;

import java.util.List;
import java.util.Collections;
import java.util.HashMap;

//import Testa.ChromFloat;

import com.TabuSearch.MySolution;
import com.softtechdesign.ga.*;

public class myGAFloat extends GAFloat {
	// 125, 10, 0.03, 10, 100, 0, 0, 0.03, pm, 0, false, true
	public myGAFloat(int chromosomeDim, 
			int populationDim,
			double crossoverProb, 
			int randomSelectionChance,
			int maxGenerations, 
			int numPrelimRuns, 
			int maxPrelimGenerations,
			double mutationProb, 
			int crossoverType, 
			int decPtsPrecision,
			boolean positiveNumOnly, 
			boolean computeStatistics, 
			MySolution initalSolution)
			throws GAException {
		super(chromosomeDim, populationDim, crossoverProb,
				randomSelectionChance, maxGenerations, numPrelimRuns,
				maxPrelimGenerations, mutationProb, crossoverType,
				decPtsPrecision, positiveNumOnly, computeStatistics);
		
		initPopulation(initalSolution);
	}

	@Override
	protected double getFitness(int iChromIndex) {

		((MyChromFloat) this.getChromosome(iChromIndex)).updateRoute();

		return ((MyChromFloat) this.getChromosome(iChromIndex)).getFitness();
		// return 0;
	}
	
	private int getDim(ChromFloat c1, ChromFloat c2){
		int i, j;
		
		for(i=chromosomeDim-1; i>=0 && c1.genes[i]<0; i--);
		for(j=chromosomeDim-1; j>=0 && c2.genes[j]<0; j--);
		
		if(j>i)
			return j+1;
		
		return i+1;
	}
	
	protected void initPopulation(MySolution sol){
		MyChromFloat[] myPop = new MyChromFloat[populationDim];
		
		MyChromFloat initialChrom = new MyChromFloat(chromosomeDim, sol);
		
		myPop[0] = new MyChromFloat(initialChrom);
		
		List<Double> geneList = initialChrom.getGenesList();
		for (int i = 1; i < populationDim; i++)
        {
            Collections.shuffle(geneList);
            initialChrom.setGenes(geneList);
            initialChrom.updateRoute();
            myPop[i] = new MyChromFloat(initialChrom);            //myPop[i] = (MyChromFloat) initialChrom.clone();
            
        }
		setChromosomes(myPop);
	}

	protected void doPartiallyMappedCrossover(ChromFloat chrom1, ChromFloat chrom2)
    {
		// in realt� i nostri cromosomi saranno da 125 geni ora per semplicit� ne considero solo 9
		// se faccio il taglio troppo ampio l'algoritmo non funziona
		
		int dim = getDim(chrom1,chrom2);
        int crossPt1 = getRandom(dim);
        int crossPt2 = getRandom(dim);
        //se il cutting point 2 � uguale al primo ne genero un altro
        while(crossPt2 == crossPt1){
            crossPt2 = getRandom(dim);
        }
		//int crossPt1=6;		// punti forzati per debug
		//int crossPt2=3;
        int max=chromosomeDim;
        int min=0;
        double tmp=0;
        
        // creazione progressiva dei figli 
        Double[] offspring = new Double[chromosomeDim];
        Double[] offspring2 = new Double[chromosomeDim];
        
        // creo due mappe nelle quali ad ogni chiave di un parent corrisponde un valore dell'altro parents
        HashMap<Double, Double> myhash = new HashMap<Double, Double>();
        HashMap<Double, Double> myhash2 = new HashMap<Double, Double>();
        
        // inizializzazione dei figli
        for(int i=0;i<chromosomeDim;i++){
        	offspring[i]=-999.0;  // imposto a -999 come valere di sicurezza
        	offspring2[i]=-999.0; // anche se non verra' mai controllato/utilizzato
        }
        
        if(crossPt1>crossPt2){max = crossPt1; min = crossPt2; }
        if(crossPt1<crossPt2){max = crossPt2; min = crossPt2; }
        
        /*if (crossPt1 != crossPt2) //tolto perch� forzo la diversit� prima
        {*/
        	// faccio il mapping dei valori all'interno della finestra
        	 
            for (int geneIndex=min; geneIndex < max; geneIndex++){
            	myhash.put(chrom1.genes[geneIndex],chrom2.genes[geneIndex]);
            	offspring[geneIndex] = chrom2.genes[geneIndex];
            	
                myhash2.put(chrom2.genes[geneIndex] ,chrom1.genes[geneIndex]);
                offspring2[geneIndex] = chrom1.genes[geneIndex];
            }
            
            for (int i=0;i<chromosomeDim;i++){
            	//if(offspring[i]==-1){
                if(i<min || i>=max){
                  if(myhash2.containsKey(chrom1.genes[i])){
                     tmp = myhash2.get(chrom1.genes[i]);
                     while (myhash2.containsKey(tmp)){		
                        tmp = myhash2.get(tmp);
                    }
                    offspring[i]=tmp;

                }
                else{
                 offspring[i]=chrom1.genes[i];
             }
            	//}
            	//if(offspring2[i]==-1){
             if(myhash.containsKey(chrom2.genes[i])){
                 tmp=myhash.get(chrom2.genes[i]);
                 while(myhash.containsKey(tmp)){
                    tmp = myhash.get(tmp);
                }
                offspring2[i]=tmp;
            }
            else{
             offspring2[i]=chrom2.genes[i];
         }
     }            
 }
        	
            
        //}*/
        for(int i=0;i<chromosomeDim;i++){
        	chrom1.genes[i] = offspring[i];
        	chrom2.genes[i] = offspring2[i];
        }
        
        ((MyChromFloat) chrom1).setUpdated(false);
        ((MyChromFloat) chrom2).setUpdated(false);
        ((MyChromFloat) chrom1).updateRoute();
        ((MyChromFloat) chrom2).updateRoute();
    }
}
