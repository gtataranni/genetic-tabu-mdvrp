package mypkg;

import com.softtechdesign.ga.*;

public class MyChromosome extends com.softtechdesign.ga.Chromosome {

	static int defaultGeneLength = 200;
	int[] genes = new int[defaultGeneLength];
	
	public int getGene(int index){
		return genes[index];
	}
	
	public void setGene(int index, int value){
		genes[index] = value;
	}
	
	public int size(){
		return genes.length;
	}
	

	@Override
	public String getGenesAsStr() {
		String geneStr = "";
		for(int i = 0; i < size(); i++){
			geneStr += getGene(i) + " ";
		}
		return geneStr;
	}

	@Override
	public void copyChromGenes(Chromosome chromosome) {
		// static length --> same size for any chromosome
		for(int i = 0; i < size(); i++){
			genes[i] = ((MyChromosome) chromosome).getGene(i);
		}
	}

	@Override
	public int getNumGenesInCommon(Chromosome chromosome) {
		int n = 0;
		for(int i = 0; i < size(); i++){
			if (genes[i] == ((MyChromosome) chromosome).getGene(i))
				n++;
		}
		return n;
	}

}
