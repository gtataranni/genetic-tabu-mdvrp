package mypkg;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.TabuSearch.MyObjectiveFunction;
import com.TabuSearch.MySolution;
import com.mdvrp.*;
import com.softtechdesign.ga.*;

public class MyChromFloat extends ChromFloat{
	
	private Route[] routes;
	private Map<Integer, Customer> customers = new HashMap<Integer, Customer>();
	private MySolution sol;
//	private double fit;
	private MyObjectiveFunction mof = new MyObjectiveFunction(MySolution.getInstance());
	private boolean updated = false;

	public MyChromFloat(int iGenesDim, MySolution solution) {
		// create chromosome genes from existing solution/routes
		super(iGenesDim); //iGenesDim = 125 (100 customers + 25 veicoli)
		this.sol = (MySolution)((MySolution)solution).clone();
		routes=(solution.getRoutes())[0];
		updated=false;
		int k = 0;
		int d = -1; //delimitatore
		for(Route r : routes)
		{
			if(!r.isEmpty()) {
					for(Customer c : r.getCustomers() ) {
						this.setGene(k++, c.getNumber());
						customers.put(c.getNumber(), c);
					}
			}
			
			//evito se il cromosoma � gi� pieno
			if (k < iGenesDim)
				this.setGene(k++, d--);  
			//mettere numeri negativi crescenti
		}
//		//riempiamo il cromosoma con delimitatori
//		if(k < iGenesDim) {
//			for(; k < iGenesDim; k++) {
//				this.setGene(k, -1);
//			}
//		} non occorre
		
		this.evaluateChrom(); 
		this.fitness = sol.getCost().getTotal();
	}
	
	public MyChromFloat(MyChromFloat mcf) {
		super(mcf.genes.length);
		this.sol = (MySolution) mcf.getSol().clone();
		this.genes = mcf.genes.clone(); 
		this.routes = mcf.getRoutes().clone();
		this.updated = mcf.isUpdated();
		this.fitness = mcf.getFitness();
		this.customers = mcf.customers;
	}
	
	public Route[] getRoutes(){
		return routes;
	}
	
	public boolean isUpdated(){
		return updated;
	}

	/**
	 * Decodifica i geni in Route e aggiorna fitness e solution
	 */
	public void updateRoute() {
		
		if(updated)
			return;
		Route[] newroutes = new Route[this.routes.length];
		// k: index to scan genes
		// i: index to scan routes
		int i=0;
		int k = 0;
		newroutes[0] = new Route();
		newroutes[0].setIndex(i);
		newroutes[0].setDepot(routes[0].getDepot());
		newroutes[0].setAssignedVehicle(routes[0].getAssignedVehicle());
//		while(genes[k] < 0)
//			k++;
		for(; k< genes.length; k++){
			if(genes[k] < 0){
				//evaluate route i
				// init new route
				i++;
				if(i < MySolution.getInstance().getVehiclesNr()) {
					newroutes[i] = new Route();
					newroutes[i].setIndex(i);
					newroutes[i].setDepot(routes[i].getDepot());
					newroutes[i].setAssignedVehicle(routes[i].getAssignedVehicle());
				}
				
			} else {
				// add customer to current route
				if(i < MySolution.getInstance().getVehiclesNr())
					newroutes[i].addCustomer(customers.get((int)genes[k]));
			}
		}
		
//		if(i!=24){
//			System.out.println("genes: "+Arrays.toString(genes));
//			System.out.println("last route: "+newroutes[i].printRoute());
//			System.out.println("!!!! i is "+i );
//		}
		
		this.routes = newroutes;
		
		Route[][] matRoutes = new Route[1][sol.getDepotVehiclesNr(0)];
		matRoutes[0] = newroutes;
		sol.setRoutes(matRoutes);
		
		this.evaluateChrom();	
	}
	
	public double getFitness(){
		//updateRoute();
		return fitness;  //gi� aggiornata nella myGAFloat (richiamo il metodo updateRoute)
	}
	
	public void evaluateChrom() {
		mof.evaluate(sol, null);
		if(sol.getCost().getTotal() < 0){
			fitness=Double.MAX_VALUE;	
//			updated = true;  //si potrebbe provare a commentare questa linea
		}
		else {
			fitness = sol.getCost().getTravelTime();  // AGGIORNATO A TRAVELTIME
//			updated = true;  //si potrebbe provare a commentare questa linea
		}
			
	}
	
	public void setUpdated(boolean updated) {
		this.updated = updated;
	}

	public MySolution getSol() {
		return sol;
	}

	public void setGenes(List<Double> geneList) {
		for(int i =0; i<genes.length; i++){
			genes[i] = geneList.get(i);
		}
		updated = false;
	}
	
    public List<Double> getGenesList()
    {
    	ArrayList<Double> al = new ArrayList<>();
    	for (double g : genes)
    		al.add(g);
        return al;
    }
    
    public void doRandomMutation() {
		double cTemp;
        int iGene1;
        int iGene2;
        
        do{
        	iGene1 = (int)(Math.random() * genes.length);
        	iGene2 = (int)(Math.random() * genes.length);
        }while(genes[iGene1] < 0 && genes[iGene2] < 0);
        	// se i geni che voglio scambiare sono entrambi non-customer (delimitatori)
        	// allora ne prendo altri due
        
        cTemp = genes[iGene1];
        genes[iGene1] = genes[iGene2];
        genes[iGene2] = cTemp;
        updated = false;
	}

}


