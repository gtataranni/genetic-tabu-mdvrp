﻿package com.mdvrp;

import java.io.FileWriter;
import java.io.PrintStream;
import java.util.Random;

import mypkg.MyChromFloat;
import mypkg.myGAFloat;

import org.coinor.opents.TabuList;

import com.TabuSearch.MyMoveManager;
import com.TabuSearch.MyObjectiveFunction;
import com.TabuSearch.MySearchProgram;
import com.TabuSearch.MySolution;
import com.TabuSearch.MyTabuList;
import com.softtechdesign.ga.Chromosome;
import com.softtechdesign.ga.Crossover;

public class MDVRPTW {
		
	private static final int  MAX = 10;
	private static final int GENES = 124;

	public static void main(String[] args) {
		MySearchProgram     search = null;
		MySolution          initialSol;
		myGAFloat			ga;
		//MyChromFloat[]		population = new MyChromFloat[MAX];
		MyObjectiveFunction objFunc;
		MyMoveManager       moveManager;
		TabuList            tabuList;
		Parameters          parameters 		= new Parameters(); 	// holds all the parameters passed from the input line
		Instance            instance; 								// holds all the problem data extracted from the input file
		Duration            duration 		= new Duration(); 		// used to calculate the elapsed time
		PrintStream         outPrintSream 	= null;					// used to redirect the output
		MyChromFloat		sol_chrom;
		
		try {			
			// check to see if an input file was specified
			parameters.updateParameters(args);
			if(parameters.getInputFileName() == null){
				System.out.println("You must specify an input file name");
				return;
			}
			
			duration.start();

			// get the instance from the file			
			instance = new Instance(parameters); 
			instance.populateFromHombergFile(parameters.getInputFileName());
			
			initialSol 		= new MySolution(instance);
			
			//init myGAFloat object
			// myGAFloat(int chromosomeDim,int populationDim,double crossoverProb,int randomSelectionChance,int maxGenerations,
			//			int numPrelimRuns,int maxPrelimGenerations,double mutationProb,int crossoverType,
			//			int decPtsPrecision,boolean positiveNumOnly,boolean computeStatistics,MySolution initalSolution)
			ga = new myGAFloat(GENES, 	6/*DEVE ESSERE PARI*/,	0.65, 	100, 	10, // si puo' provare ad aumentare ora dato che per ogni generazione i cloni vengono mutati
								0, 	0, 	0.30, 	Crossover.ctPartiallyMapped, 
								0, false, true, initialSol);
			// together with mwGAFloat construction an initial solution + other random chromosomes are inserted as initial population
			
			// tuning parameters: 
			// crossoverProb = 0.50 -> 0.65
			// mutationProb  = 0.10 -> 0.30
			// iCycle 	     = 3 -> 5

			// Init memory for Tabu Search
			objFunc 		= new MyObjectiveFunction(instance);
	        moveManager 	= new MyMoveManager(instance);
	        moveManager.setMovesType(parameters.getMovesType());
	        
	        // Tabu list
	        int dimension[] = {instance.getDepotsNr(), instance.getVehiclesNr(), instance.getCustomersNr(), 1, 1};
	        tabuList 		= new MyTabuList(parameters.getTabuTenure(), dimension);
			
	        // Other parameters
	        double bestfit = 9999; // fake initialization
	        int bestfitIndex = 0;
	        MySolution finalSol = null;
	        int tot_iCycles = parameters.getiCycles(); // default: 5
	        
	        // Start GA and TS
	        for (int iCycles = 0; iCycles < tot_iCycles; iCycles++){
	        	
	        	// Start GA
	        	ga.run();
	        	
		        MyChromFloat[] populationTS = (MyChromFloat[]) ga.getChromosomes();
		        double sumFit = 0.0;
		        
//		        // Remove clones in population  // SPOSTATO in GA, per rimuovere cloni per ogni generaz
//		        for(int i=0; i < populationTS.length; i++){
//		        	for(int j=0; j<i; j++){
//		        		if(populationTS[i].getFitness() == populationTS[j].getFitness()){
//		        			// mutate chrom i and recompute fitness
//		        			populationTS[i].doRandomMutation();
//		        			populationTS[i].updateRoute();
//		        		}
//		        	}
//		        }
//		        
//		        System.out.println("POPOLAZIONE senza cloni");
//		        for(int i=0; i < populationTS.length; i++){
//		        	System.out.println(populationTS[i].getGenesAsString() + " fitness: " + populationTS[i].getFitness());
//		        }
		        
		        // LOCAL SEARCH on every gene
		        for(int i=0; i < ga.getPopulationDim() ; i++){
		        	MySolution sol_i = populationTS[i].getSol();
		        	       
		        	// Create Tabu Search object
			        search 			= new MySearchProgram(instance, sol_i, moveManager,objFunc, tabuList, false,  outPrintSream);
			        
			        // Start solving        
			        search.tabuSearch.setIterationsToGo(parameters.getIterations()/tot_iCycles); 
			        								// cambiato il default con 3000, si puo' specificare da linea di comando
			        								// il numero di iterazioni it viene diviso per tot_iCycles di modo che 
			        								// su ogni cromosome la TS ivene eseguita it/tot_iCycles * tot_iCycles = it volte
			        search.tabuSearch.startSolving();
			        			        
			        // Update solution
			        sol_i.setCost(search.feasibleCost);
			        sol_i.setRoutes(search.feasibleRoutes);
		        	populationTS[i] = new MyChromFloat(GENES, sol_i);
		        	sumFit += populationTS[i].getFitness();
		        	// keep best solution
		        	if(i==0){ 
		        		bestfit =  search.feasibleCost.travelTime; // real initialization   //cambiato in travelTime
		        		bestfitIndex = 0;
		        	} 
		        	if (search.feasibleCost.travelTime < bestfit){
		        		bestfit = search.feasibleCost.travelTime;      //cambiato in travelTime
		        		bestfitIndex = i;
		        	}
		        }
		        
		        // RE-INSERTION with probability rip
		        MyChromFloat[] populationGA = (MyChromFloat[]) ga.getChromosomes();
		        for(int i=0; i < ga.getPopulationDim() ; i++){
		        	double rip_i = 1 - (populationTS[i].getFitness() / sumFit); // re-insertion probability RIP of cromosome i
		        	if(Math.random() < rip_i || i == bestfitIndex){ // best solution is always re-inserted
		        		populationGA[i] = populationTS[i];
		        	}
		        }
		        finalSol = populationGA[bestfitIndex].getSol();
		        ga.setChromosomes(populationGA);
		        ga.computeFitnessRankings();
	        }
	        
	        // LAST TABU to write everything in MySearchProgram search	        
	        // Create Tabu Search object
	        search 			= new MySearchProgram(instance, finalSol, moveManager,objFunc, tabuList, false,  outPrintSream);
	        // Start solving        
	        search.tabuSearch.setIterationsToGo(1000); // per la tabu finale mettiamo a 1000
	        search.tabuSearch.startSolving();
		              

	        duration.stop();

//	        for(int i = 0; i<initialSol.getRoutes().length; i++)
//	        	for(int j=0; j<initialSol.getDepotVehiclesNr(i); j++)
//	        		System.out.println("initial i: "+i+"; j: "+j+"; "+initialSol.getRoute(i, j).printRoute());
//	        
//	        for(int i =0; i < search.feasibleRoutes.length; ++i)
//	        	for(int j=0; j < search.feasibleRoutes[i].length; ++j)
//	        		System.out.println("i: "+i+"; j: "+j+"; "+search.feasibleRoutes[i][j].printRoute());
//	        
	        // Count routes
	        int routesNr = 0;
	        for(int i =0; i < search.feasibleRoutes.length; ++i)
	        	for(int j=0; j < search.feasibleRoutes[i].length; ++j)
	        		if(search.feasibleRoutes[i][j].getCustomersLength() > 0)
	        			routesNr++;
	        // Print results
	        String outSol = String.format("%s; %5.2f; %d; %4d\r\n" ,
	        		instance.getParameters().getInputFileName(), search.feasibleCost.total,
	            	duration.getMinutes()*60+duration.getSeconds(), routesNr);
	        System.out.println(outSol);
	        FileWriter fw = new FileWriter(parameters.getOutputFileName(),true);
	        fw.write(outSol);
	        fw.close();
	        
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
